# CORS

LFS preview requires CORS headers on bucket


```shell
gsutil cors set cors/google.json gs://my-bucket
aws s3api put-bucket-cors --cors-configuration file://cors/aws.json --bucket my-bucket
```
